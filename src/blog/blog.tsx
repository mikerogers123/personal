import React from "react";
import { connectComponent } from "../redux/connected-components";
import PostSummary from "./post-summary";
import Filters from "./post-filters";
import { Post } from "./redux/blog.types";
import { LoadPostsComponent } from "./load-posts";
import { addFilterAction, removeFilterAction } from "./redux/blog.actions";

class BlogComponent extends LoadPostsComponent {
  render() {
    return (
      <div className="row">
        <div className="col s12 m8 offset-m2">
          <Filters></Filters>
        </div>

        {this.filteredPosts()
          .map((p) => postSummaryComponent(p, this.storeState.blog.filters))
          .map((post) => (
            <div className="col s12 m8 offset-m2">{post}</div>
          ))}
      </div>
    );
  }

  filteredPosts() {
    const posts = this.storeState.blog.posts;
    const filters = this.storeState.blog.filters;

    const filter =
      filters.length === 0
        ? (_: Post) => true
        : (post: Post) =>
            post.categories.filter((c) => filters.includes(c)).length > 0;

    return posts.filter(filter);
  }

  modifyFilters(label: string, isActive: boolean) {
    const action = isActive ? addFilterAction : removeFilterAction;

    this.props.dispatch(action(label));
  }
}

export default connectComponent(BlogComponent);

const postSummaryComponent = (p: Post, f: ReadonlyArray<string>) => (
  <PostSummary post={p} filters={f}></PostSummary>
);
