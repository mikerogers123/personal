import moment from "moment";
import { Post } from "./redux/blog.types";

export const formattedPostDate = (date: string) => moment(date).format('MMM Do, YY');

export const toPostId = (post: Post) => guidToPostId(post.guid);

const trimFromLastOccurrenceOf = (char: string) => (str: string) => {
    const i = str.lastIndexOf(char);
    return str.substring(i+1);
}

const guidToPostId = trimFromLastOccurrenceOf('/');