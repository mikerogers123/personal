import fetch from 'node-fetch';
import { FeedResponse, FeedItem, Post } from './redux/blog.types';

const blogPostsUrl = () => toJsonUrl(blogFeedUrl('mikerogers1357'));

const toJsonUrl = (url: string) => `https://api.rss2json.com/v1/api.json?rss_url=${url}`;

const blogFeedUrl = (user: string) => `https://medium.com/feed/@${user}`;

const isBlogPost = (feedItem: FeedItem) => feedItem.categories.length > 0;

export const getPosts = () => fetch(blogPostsUrl())
    .then(res => res.json())
    .then(data => data as FeedResponse)
    .then(feed => feed.items.filter(isBlogPost) as ReadonlyArray<Post>);

