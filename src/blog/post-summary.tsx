import React from "react";
import { Post } from "./redux/blog.types";
import { Link } from "react-router-dom";
import { formattedPostDate, toPostId } from "./post.functions";

export type PostProps = Readonly<{
  post: Post;
  filters: ReadonlyArray<string>;
}>;

class PostSummaryComponent extends React.Component<PostProps> {
  render() {
    return (
      <div className="card">
        <div className="card-content">
          <span className="card-title">{this.props.post.title}</span>
          <p className="right">{formattedPostDate(this.props.post.pubDate)}</p>
          <small>
            {this.props.post.categories
              .map((c) => (
                <span className={this.highlight(c) ? "black white-text" : ""}>{c}</span>
              ))
              .map((c) => (
                <span>{c} | </span>
              ))}
          </small>
        </div>
        <div className="card-action">
          <Link to={`blog/post/${toPostId(this.props.post)}`}>Read</Link>
        </div>
      </div>
    );
  }

  highlight(category: string) {
    return this.props.filters.includes(category);
  }
}

export default PostSummaryComponent;
