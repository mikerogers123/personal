import React from 'react';
import Icon from '../icons/icon';
import { connectComponent } from '../redux/connected-components';
import { Link } from 'react-router-dom';
import { formattedPostDate } from './post.functions';
import { LoadPostsComponent } from './load-posts';
import '../styles/post.css';

class PostComponent extends LoadPostsComponent {
    render() {
        return this.post() == null ? <p>No posts here.</p> : (
            <div>
                <Link to='/blog'>
                    <Icon icon='arrow_back_ios'></Icon>
                </Link>
                <h5 className="right">{formattedPostDate(this.post()!.pubDate)}</h5>
                <div className="card">
                    <div className="card-content">

                    <span className="card-title">{this.post()!.title}</span>
                    <div dangerouslySetInnerHTML={{ __html: this.post()!.content }}></div>
                    </div>
                </div>
            </div>
        );
    }

    post() {
        const id = (this.props as any).match.params.id;

        return this.storeState.blog.posts.find(p => p.guid.includes(id!));
    }
}

export default connectComponent(PostComponent);
