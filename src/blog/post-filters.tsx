import {
  ConnectedComponent,
  connectComponent,
} from "../redux/connected-components";
import {
  addFilterAction,
  removeFilterAction,
  clearFilterAction,
} from "./redux/blog.actions";
import React from "react";
import Chip from "../chip/chip";

class PostFiltersComponent extends ConnectedComponent {
  render() {
    return (
      <div className="row">
        <div className="col s12">
          {this.filters()}
        </div>
        <div className="col s12">
          <span className="right bold pointer" onClick={() => this.clearFilters()}>
            clear filters
          </span>
        </div>
      </div>
    );
  }

  clearFilters() {
    this.dispatch(clearFilterAction({}));
  }

  filters() {
    return this.categories.map((c) => (
      <Chip
        label={c}
        onClick={this.modifyFilters(c)}
        isActive={this.isFilter(c)}
      ></Chip>
    ));
  }

  get categories() {
    const all = this.storeState.blog.posts.map((p) => p.categories).flat();

    return [...new Set(all)];
  }

  modifyFilters(label: string) {
    return () => {
      const isFilter = this.storeState.blog.filters.includes(label);
      const action = isFilter ? removeFilterAction : addFilterAction;

      this.dispatch(action(label));
    };
  }

  isFilter(category: string) {
    return this.storeState.blog.filters.includes(category);
  }
}

export default connectComponent(PostFiltersComponent);
