export type BlogStoreState = Readonly<{
    posts: ReadonlyArray<Post>,
    filters: ReadonlyArray<string> 
}>;

export const defaultBlogStoreState = () => ({posts: [], filters: []});

export interface Feed {
    url: string;
    title: string;
    link: string;
    author: string;
    description: string;
    image: string;
}

export interface Enclosure {
}

export interface Post extends FeedItem {

}

export interface FeedItem {
    title: string;
    pubDate: string;
    link: string;
    guid: string;
    author: string;
    thumbnail: string;
    description: string;
    content: string;
    enclosure: Enclosure;
    categories: string[];
}

export interface FeedResponse {
    status: string;
    feed: Feed;
    items: FeedItem[];
}
