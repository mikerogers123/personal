import { actionCreator } from "../../redux/managed-action";
import { Post } from "./blog.types";

export const loadPostsAction = actionCreator<{}>('LOAD_BLOG_POSTS');
export const loadPostsActionSuccess = actionCreator<ReadonlyArray<Post>>('LOAD_BLOG_POSTS_SUCCESS');

export const addFilterAction = actionCreator<string>('ADD_BLOG_POSTS_FILTER');
export const removeFilterAction = actionCreator<string>('REMOVE_BLOG_POSTS_FILTER');
export const clearFilterAction = actionCreator<{}>('CLEAR_BLOG_POSTS_FILTER');