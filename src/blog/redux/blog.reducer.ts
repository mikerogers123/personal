import { Action } from "redux";
import { BlogStoreState, defaultBlogStoreState } from "./blog.types";
import { matches } from "../../redux/managed-action";
import { loadPostsActionSuccess, addFilterAction, removeFilterAction, clearFilterAction } from "./blog.actions";

export default function blogReducer(state: BlogStoreState = defaultBlogStoreState(), action: Action): BlogStoreState {
    if (matches(action, loadPostsActionSuccess)) {
        return {
            ...state,
            posts: action.payload
        };
    }

    if (matches(action, addFilterAction)) {
        return {
            ...state,
            filters: [...state.filters, ...[action.payload]]
        };
    }

    if (matches(action, removeFilterAction)) {
        return {
            ...state,
            filters: state.filters.filter(filter => filter !== action.payload)
        };
    }

    if (matches(action, clearFilterAction)) {
        return {
            ...state,
            filters: []
        };
    }

    return state;
}