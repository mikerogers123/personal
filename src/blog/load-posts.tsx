import { ConnectedComponent } from "../redux/connected-components";
import { ComponentProps } from "../redux/component-props";
import { getPosts } from "./posts.service";
import { loadPostsActionSuccess } from "./redux/blog.actions";

export abstract class LoadPostsComponent extends ConnectedComponent {
    constructor(props: ComponentProps) {
        super(props);
        getPosts().then(posts => this.dispatch(loadPostsActionSuccess(posts)));
    }
}