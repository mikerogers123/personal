import React from 'react';
import { connectComponent, ConnectedComponent } from '../redux/connected-components';
import Icon from '../icons/icon';

const srcCodeLink = "https://gitlab.com/mikerogers123/personal";

export class SourceCodeLinkComponent extends ConnectedComponent {
    render() {
        return (
            <div className="fixed-action-btn hide-on-med-and-down">
                <a className="btn-floating btn-large" target="_blank" rel="noopener noreferrer" href={srcCodeLink}>
                    <Icon icon="code"></Icon>
                </a>
            </div>
        );
    }

}

export default connectComponent(SourceCodeLinkComponent);
