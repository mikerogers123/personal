import React from 'react';
import { connectComponent, ConnectedComponent } from '../redux/connected-components';
import Icon from '../icons/icon';
import { Link } from "react-router-dom";
import '../styles/navbar.css';

class NavbarComponent extends ConnectedComponent {
    render() {
        return (
            <div className="navbar">
                <nav className="transparent z-depth-0">
                    <div className="nav-wrapper">
                        <a href="/" className="brand-logo left">
                            <Icon icon="home"></Icon>
                        </a>
                        <ul id="nav-mobile" className="right">
                            <li><Link to="/about">about</Link></li>
                            <li><Link to="/blog">blog</Link></li>
                        </ul>
                    </div>
                </nav>
            </div>
        );
    }
}

export default connectComponent(NavbarComponent);
