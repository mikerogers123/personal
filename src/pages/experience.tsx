import React from "react";
import {
  ConnectedComponent,
  connectComponent,
} from "../redux/connected-components";
import Page from "./page";
import { Timeline, Event } from "react-timeline-scribble";
import { Work } from "../data/experience.data";

const interval = (work: Work) => `${work.from} - ${work.to || 'current'}`;

export class ExperienceComponent extends ConnectedComponent {
  render() {
    return (
      <Page title="Experience">
        <Timeline>
          {this.storeState.pages.experience!.map((work) => (
            <Event
              interval={interval(work)}
              title={work.name}
              subtitle={work.jobTitle}
            >
              {work.decription}
            </Event>
          ))}
        </Timeline>
      </Page>
    );
  }
}

export default connectComponent(ExperienceComponent);
