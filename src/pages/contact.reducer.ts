import { pageReducer } from "./redux/pages.reducer";
import { contact } from "../data/contact.data";

export default pageReducer(contact);