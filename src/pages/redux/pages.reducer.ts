import { combineReducers, Action } from "redux";
import { default as aboutReducer } from "../about.reducer";
import { default as contactReducer } from "../contact.reducer";
import { default as eduactionReducer } from "../education.reducer";
import { default as experienceReducer } from "../experience.reducer";
import { default as skillsReducer } from "../skills.reducer";
import { PageStoreState, defaultPageStoreState } from "./pages.types";
import { matches } from "../../redux/managed-action";
import { loadPagesAction } from "./pages.actions";

export default combineReducers({
    about: aboutReducer,
    contact: contactReducer,
    education: eduactionReducer,
    experience: experienceReducer,
    skills: skillsReducer
});

export function pageReducer<TPage>(
    data: TPage,
    innerReducer: (state: TPage, action: Action) => TPage = (s, _) => s
) {
    return (state: PageStoreState<TPage> = defaultPageStoreState(), action: Action): PageStoreState<TPage> => {
        
        if (matches(action, loadPagesAction)) {
            return data;
        }
        
        return innerReducer(state!, action);

    }
}