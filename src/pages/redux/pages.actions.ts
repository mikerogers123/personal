import { actionCreator } from "../../redux/managed-action";

export const loadPagesAction = actionCreator<{}>('LOAD_PAGES');