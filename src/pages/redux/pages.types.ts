import { About } from "../../data/about.data";
import { CategorisedSkills } from "../../data/skills.data";
import { ContactMedium } from "../../data/contact.data";
import { Education } from "../../data/education.data";
import { Work } from "../../data/experience.data";

export type PagesStoreState = Readonly<{
    about: PageStoreState<About>,
    contact: PageStoreState<ReadonlyArray<ContactMedium>>,
    education: PageStoreState<Education>,
    experience: PageStoreState<ReadonlyArray<Work>>,
    skills: PageStoreState<CategorisedSkills>
}>

export type PageStoreState<TPage> = TPage | null;

export const defaultPagesStoreState = () => ({
    about: defaultPageStoreState(),
    contact: [],
    education: defaultPageStoreState(),
    experience: [],
    skills: defaultPageStoreState()
});

export const defaultPageStoreState = <TPage>(): PageStoreState<TPage> => null;