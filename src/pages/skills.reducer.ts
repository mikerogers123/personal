import { pageReducer } from "./redux/pages.reducer";
import { skills } from "../data/skills.data";

export default pageReducer(skills);