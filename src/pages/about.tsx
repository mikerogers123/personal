import React from 'react';
import { ConnectedComponent, connectComponent } from '../redux/connected-components';
import Page from './page';
import ReactJson from 'react-json-view';

export class AboutComponent extends ConnectedComponent {
    render() {
        return (
            <Page title="About">
                <div className="z-depth-2">
                    <ReactJson src={this.storeState.pages.about || {}} name="me" theme="twilight" collapsed={true}></ReactJson>
                </div>
            </Page>
        );
    }
}

export default connectComponent(AboutComponent);
