import React from "react";
import Card from "../card/card";
import {
  ConnectedComponent,
  connectComponent,
} from "../redux/connected-components";
import UniImage from "../images/uni.png";
import CollegeImage from "../images/college.png";
import SchoolImage from "../images/school.jpg";
import Page from "./page";
import { Education, GcseGrade } from "../data/education.data";
import '../styles/timeline.css';

export class EducationComponent extends ConnectedComponent {
  render() {
    return (
      <Page title="Education">
        <div className="row">
          <div className="col s12 m4">
            <Card
              description={this.universityDescription}
              descriptionTitle={this.degreeSummary}
              image={UniImage}
              title="University"
            ></Card>
          </div>

          <div className="col s12 m4">
            <Card
              description={this.collegeDescription}
              descriptionTitle={this.collegeSummary}
              image={CollegeImage}
              title="A-Levels"
            ></Card>
          </div>

          <div className="col s12 m4">
            <Card
              description={this.schoolDescription}
              descriptionTitle={this.schoolSummary}
              image={SchoolImage}
              title="GCSEs"
            ></Card>
          </div>
        </div>
      </Page>
    );
  }

  get schoolSummary() {
    return this.coalesce((education) => education.school.location, "");
  }

  get collegeSummary() {
    return this.coalesce((education) => education.college.location, "");
  }

  get degreeSummary() {
    return this.coalesce(
      (education) =>
        `${education.degree.hons} (Hons) ${education.degree.name} ${education.degree.level}`,
      ""
    );
  }

  get universityDescription() {
    return this.coalesce(
      (education) => (
        <div>
          <p className="bold">{education.degree.location}</p>
          {education.degree.awards.map((award) => (
            <div>{award}</div>
          ))}
        </div>
      ),
      <p></p>
    );
  }

  get collegeDescription() {
    return this.coalesce(
      (education) => (
        <div>
          <p>
            <span className="bold">A-Levels</span>{" "}
            {education.college.aLevels
              .map((aLevel) => `${aLevel.grade} in ${aLevel.name}`)
              .join(" | ")}
          </p>
          <p>
            <span className="bold">AS-Levels</span>{" "}
            {education.college.asLevels
              .map((asLevel) => `${asLevel.grade} in ${asLevel.name}`)
              .join(" | ")}
          </p>
        </div>
      ),
      <p></p>
    );
  }

  get schoolDescription() {
    return this.coalesce(
      (education) => (
        <div>
          <p>
              {Object.keys(education.school.count).map(grade => `${education.school.count[grade as GcseGrade]} ${grade}`).join(', ')} 
          </p>

          <p>
              Includes {education.school.notableMentions.map(mention => `${mention.grade} in ${mention.name}`).join(', ')}
          </p>
        </div>
      ),
      <p></p>
    );
  }

  coalesce<TReturn>(fn: (state: Education) => TReturn, def: TReturn) {
    const education = this.storeState.pages.education;

    if (education == null) {
      return def;
    }

    return fn(education);
  }
}

export default connectComponent(EducationComponent);
