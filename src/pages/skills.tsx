import React from 'react';
import { ConnectedComponent, connectComponent } from '../redux/connected-components';
import Page from './page';
import ProgressBar from '../progress-bar/progress-bar';
import { CategorisedSkills } from '../data/skills.data';

export class SkillsComponent extends ConnectedComponent {
    render() {
        return (
            <Page title="Skills">
                {this.categories.map(cat => categorisedSkillComponent(this.categorisedSkills, cat))}
            </Page>
        );
    }

    get categories() {
        return Object.keys(this.categorisedSkills);
    }

    get categorisedSkills() {
        return this.storeState.pages.skills || {};
    }
}


const categorisedSkillComponent = (skills: CategorisedSkills, category: string) => {
    return (
        <div className="card">
            <div className="row card-content">
                <span className="card-title">{category}</span>
                {proficiencies(skills, category)}
                <div className="col s12">
                    <span className="bold">Tools used in anger</span> {tooling(skills, category)}.
                </div>
            </div>
        </div>);
}

const proficiencies = (skills: CategorisedSkills, category: string) => {
    return skills[category].proficiencies.map(skill =>
        <div className="col s12 m6">
            <ProgressBar progress={skill.proficiency} label={skill.name}></ProgressBar>
        </div>
    )
}

const tooling = (skills: CategorisedSkills, category: string) => {
    return skills[category].tooling.join(' | ');
}

export default connectComponent(SkillsComponent);
