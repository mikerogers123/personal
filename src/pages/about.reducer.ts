import { pageReducer } from "./redux/pages.reducer";
import { about } from "../data/about.data";

export default pageReducer(about);