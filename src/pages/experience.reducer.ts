import { pageReducer } from "./redux/pages.reducer";
import { experience } from "../data/experience.data";

export default pageReducer(experience);