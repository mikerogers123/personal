import { pageReducer } from "./redux/pages.reducer";
import { education } from "../data/education.data";

export default pageReducer(education);