import React from "react";
import {
  ConnectedComponent,
  connectComponent,
} from "../redux/connected-components";
import Page from "./page";
import ButtonLink from "../buttons/button-link";

export class ContactComponent extends ConnectedComponent {
  render() {
    return (
      <Page title="Contact">
        <div className="row">
          {this.storeState.pages.contact!.map((item) => (
            <div className="center-align col s12 m4">
              <ButtonLink link={item.link}>{item.name}</ButtonLink>
              <p>{item.description}</p>
            </div>
          ))}
        </div>
      </Page>
    );
  }
}

export default connectComponent(ContactComponent);
