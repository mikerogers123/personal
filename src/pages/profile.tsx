import React from "react";
import Me from "../images/blackwhite.jpg";
import "../styles/profile.css";
import { cvLink } from "../cv-download/cv-download";


export class ProfileComponent extends React.Component {
    render() {
        return (
            <div className="profile">

                <div className="row">
                    <div className="col s12 m8 offset-m2 l6 offset-l3">
                        <div className="card-panel z-depth-1">
                            <div className="row valign-wrapper">
                                <div className="col s2">
                                    <img src={Me} alt="me" className="circle responsive-img" />
                                </div>
                                <div className="col s10">
                                    <span className="black-text">
                                        Full stack developer. Passionate about clean code and software design. Always learning by reading and trying new things.
                                </span>
                                </div>
                            </div>
                            <div className="card-action">
                                <a href={cvLink} target="_blank" rel="noopener noreferrer">My full resume here</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ProfileComponent;
