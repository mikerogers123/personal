import React from 'react';
import { connectComponent, ConnectedComponent } from '../redux/connected-components';
import Education from '../pages/education';
import Experience from '../pages/experience';
import About from '../pages/about';
import Contact from '../pages/contact';
import CvDownload from '../cv-download/cv-download';
import Profile from '../pages/profile';

class AboutComponent extends ConnectedComponent {
    render() {
        return (
            <div>
                <Profile></Profile>
                <About></About>
                <Experience></Experience>
                <Education></Education>
                <Contact></Contact>
                <CvDownload></CvDownload>
            </div>
        );
    }
}

export default connectComponent(AboutComponent);
