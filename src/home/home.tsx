import React from "react";
import {
  connectComponent,
  ConnectedComponent,
} from "../redux/connected-components";
import Me from "../images/blackwhite.jpg";
import SourceCodeLink from "../source-code/source-code-link";
import SnakeGame from "../snake-game/snake-game";
import { ComponentProps } from "../redux/component-props";
import { loadPagesAction } from "../pages/redux/pages.actions";

class HomeComponent extends ConnectedComponent {
  constructor(props: ComponentProps) {
    super(props);
    this.dispatch(loadPagesAction({}));
  }
  
  render() {
    return (
      <React.Fragment>
        <div className="center-align hide-on-med-and-down">
          <SnakeGame></SnakeGame>
        </div>

        <div className="center-align home-content">
          <SourceCodeLink></SourceCodeLink>
          <div>
            <img className="profile-pic circle responsive-img" src={Me} alt="me, myself and I"></img>
          </div>
          <div>
            <h1>MIKE ROGERS</h1>
          </div>
          <div>
            <h5>software engineer</h5>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default connectComponent(HomeComponent);
