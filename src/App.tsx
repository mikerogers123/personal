import React from "react";
import { connectComponent, ConnectedComponent } from "./redux/connected-components";
import Navbar from "./navbar/navbar";
import { Switch, Route } from "react-router-dom";
import Home from "./home/home";
import About from "./about/about";
import Blog from "./blog/blog";
import BlogPost from "./blog/post";
import './styles/typography.css';

export class AppComponent extends ConnectedComponent {
  render() {
    return (
      <React.Fragment>
        <div className="container">
          <Navbar></Navbar>
        </div>

        <Switch>
          <Route exact path="/" component={Home}/>
        </Switch>

        <div className="container">
          <Switch>
            <Route exact path="/about" component={About}/>
            <Route exact path="/blog" component={Blog}/>
            <Route exact path="/blog/post/:id" component={BlogPost}/>
          </Switch>
        </div>
      </React.Fragment>
    );
  }
}

export default connectComponent(AppComponent);
