import React from "react";

export default class ChipComponent extends React.Component<
  { label: string; isActive: boolean; onClick: () => void }
> {
  constructor(props: any) {
    super(props);
    this.state = { isActive: this.props.isActive };
    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    this.props.onClick();
  }

  render() {
    return (
      <div
        className={`chip pointer ${this.props.isActive ? "black white-text" : "white"}`}
        onClick={() => this.onClick()}
      >
        {this.props.label}
      </div>
    );
  }
}
