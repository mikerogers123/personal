import { SnakeGame } from "react-game-snake";
import React from "react";
import {
  connectComponent,
  ConnectedComponent,
} from "../redux/connected-components";

export class SnakeGameComponent extends ConnectedComponent {
  game() {
    return (
      <SnakeGame
        colors={{
          field: "darkslategrey",
          food: "lightslategrey",
          snake: "whitesmoke",
        }}
        countOfHorizontalFields={30}
        countOfVerticalFields={10}
        fieldSize={20}
        loopTime={200}
        pauseAllowed={true}
        restartAllowed={true}
      ></SnakeGame>
    );
  }

  help() {
    return (
      <p title="Move the snake with the arrow keys. P to pause. R to restart.">
        &#9432;
      </p>
    );
  }

  render() {
    return (
      <React.Fragment>
        {this.game()}
        {this.help()}
      </React.Fragment>
    );
  }
}

export default connectComponent(SnakeGameComponent);
