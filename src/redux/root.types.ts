import { PagesStoreState, defaultPagesStoreState } from "../pages/redux/pages.types";
import {BlogStoreState, defaultBlogStoreState} from "../blog/redux/blog.types";

export type StoreState = Readonly<{
    pages: PagesStoreState,
    blog: BlogStoreState
}>;

export const initialState = {
    pages: defaultPagesStoreState(),
    blog: defaultBlogStoreState()
} as StoreState;