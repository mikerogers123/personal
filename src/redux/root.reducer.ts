import { combineReducers } from "redux";
import { default as pagesReducer } from "../pages/redux/pages.reducer";
import { default as blogReducer } from "../blog/redux/blog.reducer";

export default combineReducers({
    pages: pagesReducer,
    blog: blogReducer
}); 