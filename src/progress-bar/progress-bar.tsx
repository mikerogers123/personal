import React from 'react';

type ProgressBarProps = Readonly<{
    label: string,
    progress: number
}>

class ProgressBarComponent extends React.Component<ProgressBarProps> {
    render() {
        return (
            <div className="row">
                <div className="col s4">
                    <span className="right">{this.props.label}</span>
                </div>
                <div className="col s8">
                    <div className="progress">
                        <div className="determinate" style={{ width: this.progressString }}></div>
                    </div>
                </div>
            </div>
        );
    }

    get progressString() {
        return this.props.progress + '%';
    }
}

export default ProgressBarComponent;
