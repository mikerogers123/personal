import moment from "moment";

export const about = {
    forename: 'Michael',
    middleName: 'James',
    lastName: 'Rogers',
    born: moment("19950310", "YYYYMMDD").fromNow(),
    relationship: {
        married: moment("20160907", "YYYYMMDD").fromNow(),
        gotTogether: moment("20100717", "YYYYMMDD").fromNow(),
        children: [
            {
                name: 'Luna',
                born: moment("20180430", "YYYYMMDD").fromNow()
            }
        ]
    },
    hobbies: [
        'snooker',
        'ping pong',
        'badminton',
        'PS4',
        'football',
        'gym',
        'not DIY'
    ],
    childhoodDreamJob: 'snooker player',
    proudestMoments: [
        'Wedding day',
        'Daughter\'s birth',
        'Graduating',
        'Representing the England U16 snooker team',
        'Playing Jimmy White in a competitive snooker match'
    ]
}

export type About = Readonly<{
    forename: string,
    middleName: string,
    lastName: string,
    born: string,
    relationship: {
        married: string,
        gotTogether: string,
        children: [
            {
                name: 'Luna',
                born: string
            }
        ]
    },
    hobbies: ReadonlyArray<string>,
    childhoodDreamJob: string,
    proudestMoments: ReadonlyArray<string>
}>
