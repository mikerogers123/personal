export const education = {
    degree: {
        name: 'Mathematics',
        level: '1st',
        hons: 'BSc',
        awards: [
            'David Powell - Highest Scoring Applied Matemeatics Undergraduate',
            'BP Cenurion Award - Highest Achieving 2nd Year Student'
        ],
        location: 'University of Bath'
    },
    college: {
        location: 'Richard Huish College',
        aLevels: [
            {name: 'Mathematics', grade: 'A*'},
            {name: 'Further Mathematics', grade: 'A*'},
            {name: 'Physics', grade: 'A*'}
        ],
        asLevels: [{name: 'Accounting', grade: 'A'}]
    },
    school: {
        location: 'Heathfield Community School',
        count: {
            'A*': 9,
            'A': 3,
            'B': 1
        },
        notableMentions: [
            {name: 'Mathematics', grade: 'A*'},
            {name: 'Science', grade: 'A*'},
            {name: 'English Literature', grade: 'A'},
            {name: 'English Language', grade: 'A*'},
        ]
    }
}

export type Education = Readonly<{
    degree: {
        name: string,
        level: DegreeLevel,
        hons: Hons,
        awards: ReadonlyArray<string>,
        location: string
    },
    college: {
        location: string,
        aLevels: ReadonlyArray<{name: string, grade: ALevelGrade}>,
        asLevels: ReadonlyArray<{name: string, grade: ALevelGrade}>
    },
    school: {
        location: string,
        count: Record<GcseGrade, number>,
        notableMentions: ReadonlyArray<{name: string, grade: GcseGrade}>
    }

}>

type DegreeLevel = '1st' | '2:1' | '2:2' | '3rd';
type Hons = 'BSc' | 'MSc' | 'PHD';
type ALevelGrade = 'A*' | 'A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'U';
export type GcseGrade = 'A*' | 'A' | 'B' | 'C' | 'D' | 'E' | 'F';