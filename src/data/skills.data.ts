export const skills: CategorisedSkills = {
    'Backend': {
        proficiencies: [
            { name: 'C#', proficiency: 90 },
            { name: 'F#', proficiency: 50 },
            { name: 'node.js', proficiency: 40 },
            { name: 'git', proficiency: 70 },
            { name: 'python', proficiency: 40 }
        ],
        tooling: [
            'AutoMapper',
            'NServiceBus',
            'AutoFaq',
            'Castle Windsor',
            'NUnit',
            'ASP.NET Framework & Web API',
            '.NET Core',
            'pandas',
            'numpy'
        ]
    },
    'Database': {
        proficiencies: [
            { name: 'SQL', proficiency: 50 },
            { name: 'Redis', proficiency: 10 },
            { name: 'Azure Table Storage', proficiency: 30 },
            { name: 'EventStore', proficiency: 40 },
            { name: 'CosmosDB', proficiency: 50 }
        ],
        tooling: [
            'Kibana',
            'Azure',
            'Entity Framework'
        ]
    },
    'Frontend': {
        proficiencies: [
            { name: 'HTML', proficiency: 50 },
            { name: 'CSS', proficiency: 20 },
            { name: 'JavaScript', proficiency: 70 },
            { name: 'Angular', proficiency: 70 },
            { name: 'React', proficiency: 30 },
        ],
        tooling: [
            'TypeScript',
            'Boostrap',
            'MaterializeCSS',
            'Font Awesome',
            'RxJS',
            'Cypress',
            'Jasmine',
            'Redux'
        ]
    },
    'Patterns & Architecture': {
        proficiencies: [
            { name: 'MicroServices', proficiency: 80 },
            { name: 'Event Sourcing', proficiency: 40 },
            { name: 'CQRS', proficiency: 40 },
            { name: 'Dependency Injection', proficiency: 70 }
        ],
        tooling: []
    }
}

export type Skills = {
    proficiencies: ReadonlyArray<Proficiency>,
    tooling: ReadonlyArray<string>
}

export type CategorisedSkills = Record<string, Skills>

export type Proficiency = Readonly<{ name: string, proficiency: number }>