export const experience: ReadonlyArray<Work> = [
  {
    from: 2019,
    name: "UK Hydrographic Office",
    jobTitle: "Senior engineer",
    decription: `Leveraging Event Sourcing and CQRS design patterns to implement a distributed system. Building an Azure based enterprise event service as a communication vehicle between different platforms. Providing some support for business-critical legacy systems. All implemented primarily in .NET Core, using Azure to support deployments.`,
  },
  {
    from: 2017,
    to: 2019,
    name: "Bud Systems",
    jobTitle: "Junior/mid-level engineer",
    decription: `Worked on a single page Angular 6 web application, with a microservices backend architecture implemented using NServiceBus and ASP.NET. Adopted many technologies/patterns such as Typescript, Redux & Entity Framework, using multiple database technologies including CosmosDB, Azure Table Storage and SQL Server. Also obtained valuable exposure to some functional programming techniques, implemented in F#.`,
  },
  {
    from: 2016,
    to: 2017,
    name: "Civica Digital",
    jobTitle: "Graduate engineer",
    decription: `Worked in a fast-paced, agile environment following key practises such as continuous integration/delivery and unit testing. Mostly worked with ASP.NET Web Forms, C#, JavaScript, HTML & SQL.`,
  },
];

export type Work = Readonly<{
  from: number;
  to?: number;
  name: string;
  jobTitle: string;
  decription: string;
}>;
