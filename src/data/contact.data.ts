export const contact: ReadonlyArray<ContactMedium> = [
    {name: 'LinkedIn', link: 'https://www.linkedin.com/in/michael-james-rogers', description: 'A popular view my professional identity.'},
    {name: 'GitHub', link: 'https://github.com/mikerogers123', description: 'A few academic examples of my work.'},
    {name: 'Email', link: 'mailto:mikerogers_@hotmail.co.uk', description: 'Get personal.'}
];

export type ContactMedium = Readonly<{
    link: string,
    name: string,
    description: string
}>