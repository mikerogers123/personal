import React from 'react';
import { connectComponent, ConnectedComponent } from '../redux/connected-components';
import Icon from '../icons/icon';

export class CvDownloadComponent extends ConnectedComponent {
    render() {
        return (
            <div className="fixed-action-btn hide-on-med-and-down">
                <a className="btn-floating btn-large" href={cvLink} target="_blank" rel="noopener noreferrer">
                    <Icon icon="file_download"></Icon>
                </a>
            </div>
        );
    }

}

export const cvLink = "https://docs.google.com/file/d/11-ugW49UXIdcBA_GRferaqVWhBww_imx/edit?usp=docslist_api&filetype=msword";

export default connectComponent(CvDownloadComponent);
