## Description
Source code for my personal website which has two main sections: a resumé and a blog.

### Resumé
Basically a summary of what is on my professional CV, which is downloadable from the page itself.

### Blog
The blog is linked to my Medium account (for now at least). This was for ease of managing the content.

I retrieve my blog content via Medium's handy API.

### Tech used
The app is made using React, Redux and TypeScript. React is not familiar to me so this was an excuse to try it out.

The app is deployed via GitLab's CI/CD pipelines and releases occur on every push to master.

## TODO
See [trello board](https://trello.com/b/xmPdrVCi/personal-site) for ideas and progress.

## Available Scripts

In the project directory, you can run:

### npm install

Installs project dependencies. A pre-requisiste for running.

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
